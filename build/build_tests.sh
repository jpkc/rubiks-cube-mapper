#!/bin/bash
g++ -I /usr/include/eigen3/ ../src/eigentest.cpp -o ../bin/tests/testeigen.out
g++ -I /usr/include/eigen3/ ../src/cube_board_io.cpp  -o ../bin/tests/cube_board_io.out
g++ -I /usr/include/eigen3/ ../src/CubeBoardTest.cpp ../src/CubeBoard.cpp  ../bin/obj/Board.o -o ../bin/tests/CubeBoardTest.out
g++ -I /usr/include/eigen3/ ../src/tests/TestBoard.cpp ../src/CubeBoard.cpp ../bin/obj/Board.o -o ../bin/tests/TestBoard.out
