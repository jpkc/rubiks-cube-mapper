#!/bin/bash

# Squashing commits:
# git reset --soft HEAD~3
# git commit -m "Message"
# Consider doing this locally only to avoid erasing the repo history. In case you need to push changes:
# git push --force-with-lease

