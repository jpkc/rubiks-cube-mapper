#include <iostream>
#include <Eigen/Dense>
 
using Eigen::MatrixXd;

int main()
{
    int board_size = 6 * 9;
 
    MatrixXd cube(board_size,1);    
    MatrixXd permutation = MatrixXd::Identity(board_size, board_size);

    for(int i = 0; i < board_size; ++i)
        cube(i, 0) = i+1;

    std::cout << "Cube: " << std::endl << cube << std::endl << std::endl;
    std::cout << "Permutation Matrix: " << std::endl << permutation << std::endl << std::endl;

    std::cout << "Permutated Cube: " << std::endl << permutation * cube << std::endl << std::endl;
}
