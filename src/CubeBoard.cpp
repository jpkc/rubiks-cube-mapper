#include "CubeBoard.h"
#include <iomanip>
#include <sstream>

std::stringstream BlockPrint(std::array<int,BOARD_SIZE> &board, int i0) {
    std::stringstream output;

    for(int j = 0; j < 3; j++) {
        output << "\t\t\t";

        for(int i = 0; i < 3; i++) {
            output << "\t" << std::setw(4) << board[i0++];
        }
        output << std::endl;
    }
    output << std::endl;
    return output;
}

std::stringstream BlockGroupPrint(std::array<int,BOARD_SIZE> &board, int i0) {
    std::stringstream output;
    for(int k = 0; k++ < 3; output << "\n") {
        for(int j = 0; j < 3; j++) {
            for(int i = 0; i++ < 3; output << "\t") {
                output << std::setw(4) << board[i0++];
            }
            output << "\t";
        }
        output << std::endl;
    }
    output << std::endl;
    return output;
}

CubeBoard::CubeBoard(int cube_side) : side(cube_side)
{

}

CubeBoard::~CubeBoard()
{

}

std::string CubeBoard::IdentityCube()
{
    std::stringstream cube;
    std::array<int,BOARD_SIZE> board;

    for (int i = 0; i < BOARD_SIZE; board[i++] = i+1);

    cube << BlockPrint(board, 9*0).str();
    cube << BlockGroupPrint(board, 9*1).str();
    cube << BlockPrint(board, 9*4).str();
    cube << BlockPrint(board, 9*5).str();

    return cube.str();
}

std::string CubeBoard::BoardToCube(Board cubeBoard)
{
    std::stringstream cube;
    std::array<int,BOARD_SIZE> board;

    board = cubeBoard.get();

    for (int i = 0; i < BOARD_SIZE; board[i++] = i+1);

    cube << BlockPrint(board, 9*0).str();
    cube << BlockGroupPrint(board, 9*1).str();
    cube << BlockPrint(board, 9*4).str();
    cube << BlockPrint(board, 9*5).str();

    return cube.str();
}

Board CubeBoard::CubeToBoard(std::string cubeString)
{
    Board cube;
    return cube;
}

int CubeBoard::Side()
{
    return side;
}
