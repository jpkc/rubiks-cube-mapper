#ifndef CUBEBOARD_H
#define CUBEBOARD_H

#pragma once

#include "Board.h"
#include <string>

class CubeBoard
{
public:
    CubeBoard(int cube_side = 3);
    ~CubeBoard();
    std::string IdentityCube();
    std::string BoardToCube(Board cubeBoard);
    Board CubeToBoard(std::string cubeString);
    int Side();

private:
    int side;
};

#endif