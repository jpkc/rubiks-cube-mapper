#ifndef BOARDSIZE_H
#define BOARDSIZE_H

#pragma once

#define BOARD_SIZE 6*9 //Hardcoding for now. Needs to be templated for different sized boards on runtime

#endif
