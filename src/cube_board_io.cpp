#include <iostream>
#include <iomanip>

#include <fstream>

#include<array>

#define BOARD_SIZE 6*9

using namespace std;

void block_print(array<int,BOARD_SIZE> &board, int i0) {
    for(int j = 0; j < 3; j++) {
        std::cout << "\t\t\t";
        for(int i = 0; i < 3; i++) {
            std::cout << "\t" << std::setw(4) << board[i0++];
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void block_group_print(array<int,BOARD_SIZE> &board, int i0) {
    for(int k = 0; k++ < 3; std::cout << "\n") {
        for(int j = 0; j < 3; j++) {
            for(int i = 0; i++ < 3; std::cout << "\t") {
                std::cout << std::setw(4) << board[i0++];
            }
            std::cout << "\t";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

int main () {
    array<int,BOARD_SIZE> board;

    for (int i = 0; i < BOARD_SIZE; board[i++] = i+1);

    block_print(board, 9*0);
    block_group_print(board, 9*1);
    block_print(board, 9*4);
    block_print(board, 9*5);

    return 0;
}
