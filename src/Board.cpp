#include "Board.h"

Board::Board()
{
    for(int i = 0; i < BOARD_SIZE; boardArray[i++] = i + 1);
}

Board::~Board()
{

}

int Board::set(std::array<int, BOARD_SIZE> board) 
{
    boardArray = board;
    return 1;
}

std::array<int, BOARD_SIZE> Board::get() 
{
    return boardArray;
}

int Board::size() 
{
    return BOARD_SIZE;
}
