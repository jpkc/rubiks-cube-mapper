#ifndef BOARD_H
#define BOARD_H

#pragma once

#include "BoardSize.h"
#include <array>

class Board
{
public:
    Board();
    ~Board();

    int set(std::array<int, BOARD_SIZE> board);
    std::array<int, BOARD_SIZE> get();
    int size();

private:
    std::array<int, BOARD_SIZE> boardArray;
};

#endif