#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <random>

#define BASE 18

//	jpkc Rubik's cube permutations study.
//
//	The idea is:
//		1: find a way to map permutatios
//		2: find equivalent permutation sequences
//		3: use this to find permutatios that helps me solve the cube as fast as hell
//
//	Once done extend this to permutations other than Rubik's cube and conquer the world. :-)

//cube representation:
//
//     AAA       0
//     AAA       1
//     AAA       2
//               3
// BBB CCC DDD   4
// BBB CCC DDD   5
// BBB CCC DDD   6
//               7
//     EEE       8
//     EEE       9
//     EEE      10
//              11
//     FFF      12
//     FFF      13
//     FFF      14
// 01234567890

enum Moves {
	PERMUTATION_E3R = -9,	//NOTE: This enum order is important. Negatives are inverse permutations and must be treated as such.
	PERMUTATION_E2R,		//NOTE: Those permutation names are lame. Might be usefull to use some group theory naming someday.
	PERMUTATION_E1R,
	PERMUTATION_B3R,
	PERMUTATION_B2R,
	PERMUTATION_B1R,
	PERMUTATION_A3D,
	PERMUTATION_A2D,
	PERMUTATION_A1D,
	PERMUTATION_IDENTITY = 0,
	PERMUTATION_A1U,
	PERMUTATION_A2U,
	PERMUTATION_A3U,
	PERMUTATION_B1L,
	PERMUTATION_B2L,
	PERMUTATION_B3L,
	PERMUTATION_E1L,
	PERMUTATION_E2L,
	PERMUTATION_E3L = 9,
	PERMUTATION_MAX
};

typedef struct {
		char state[15][12]; //Holds cube representation
		std::vector<Moves> moves; //Holds permutation list
} Cube;

//Permutation Id
std::vector<char> IdToPermutation(unsigned long int id);
unsigned long int PermutationToId(std::vector<char> permutation);

//Cube IO
Cube Reset();
Cube SetCornersSwap();
Cube CubeRead(std::string file_path);
void CubeWrite(Cube cube, std::string file_path);
std::vector<std::vector<char> > PermutationsLoad(std::string permutations_file);
void PermutationsSave(std::vector<std::vector<char> > permutations, std::string permutations_file);

//Cube drawing
void DrawCube(Cube cube);
//void DrawCubeCV(Cube state);	//TODO: OpenCV State Draw
//void DrawCubeGL(Cube state);	//TODO: OpenGL State Draw
bool StateEval(Cube cube, bool draw = true);
void DrawPermutation(std::vector<char> permutation);
void DrawPermutations(std::vector<std::vector<char> > permutations_vector);
std::string PermutationTranslate(Moves move);

//18 Rubik's 3x3x3 cube basic permutations
Cube ApplyInverse(Cube cube, Moves permutation);
Cube Apply(Cube cube, std::vector<char> permutations);
Cube Apply(Cube cube, char permutation);
Cube Apply(Cube cube, Moves permutation);

Cube A1U(Cube cube);
Cube A1D(Cube cube);
Cube A2U(Cube cube);
Cube A2D(Cube cube);
Cube A3U(Cube cube);
Cube A3D(Cube cube);

Cube B1L(Cube cube);
Cube B1R(Cube cube);
Cube B2L(Cube cube);
Cube B2R(Cube cube);
Cube B3L(Cube cube);
Cube B3R(Cube cube);

Cube E1L(Cube cube);
Cube E1R(Cube cube);
Cube E2L(Cube cube);
Cube E2R(Cube cube);
Cube E3L(Cube cube);
Cube E3R(Cube cube);

//Solved status check
bool SolvedSide(Cube cube, int offset_y, int offset_x);
bool SolvedSide(Cube cube, char side);
bool SolvedCorners(Cube cube, char side);
bool SolvedCorners(Cube cube);
bool SolvedEdges(Cube cube, char edge);
bool SolvedEdges(Cube cube);
bool Solved(Cube cube);

//Cube Scramble
Cube Scramble(Cube cube);

//Cube Solve
Cube TraceBack(Cube cube);
Cube Solve(Cube cube);	//TODO: "Just" Implement Solve... :-)

//Cube map
void IdentityMap(unsigned long int last_mapped_permutation_id);
std::vector<std::vector<char> > SolutionsMap(Cube original_cube, std::vector<std::vector<char> > identity_permutations, unsigned long int n, unsigned long int ini = 0); //TODO: Paralelize this
bool CheckIdentitySubsequence(std::vector<char> permutation, std::vector<std::vector<char> > identity_permutations);

//----------------------------------------

Cube Reset() {
	Cube cube;
	for(int j = 0; j < 15; ++j) {
		cube.state[j][11] = 0;
	}
	for(int j = 0; j < 15; ++j) {
		for(int i = 0; i < 11; ++i) {
			cube.state[j][i] = ' ';
		}
	}
	for(int j = 0; j < 3; ++j) {
		for(int i = 4; i < 7; ++i) {
			cube.state[j][i] = 'A';
		}
	}
	for(int j = 4; j < 7; ++j) {
		for(int i = 0; i < 3; ++i) {
			cube.state[j][i] = 'B';
		}
	}
	for(int j = 4; j < 7; ++j) {
		for(int i = 4; i < 7; ++i) {
			cube.state[j][i] = 'C';
		}
	}
	for(int j = 4; j < 7; ++j) {
		for(int i = 8; i < 11; ++i) {
			cube.state[j][i] = 'D';
		}
	}
	for(int j = 8; j < 11; ++j) {
		for(int i = 4; i < 7; ++i) {
			cube.state[j][i] = 'E';
		}
	}
	for(int j = 12; j < 15; ++j) {
		for(int i = 4; i < 7; ++i) {
			cube.state[j][i] = 'F';
		}
	}
	return cube;
}

Cube SetCornersSwap() {
	Cube cube = Reset();

	cube = A1D(cube);
	cube = B1R(cube);
	cube = A3D(cube);
	cube = B1L(cube);
	cube = A1U(cube);
	cube = B1R(cube);
	cube = A3U(cube);
	cube = B1L(cube);

	return cube;
}

Cube CubeRead(std::string file_path) {
	Cube cube;

	std::vector<char> cube_state;
	std::ifstream file_in(file_path, std::ios_base::binary);
	if (!file_in)	{
		throw std::runtime_error("error opening file");
	}

	std::istreambuf_iterator<char> iter(file_in);
	std::copy(iter, std::istreambuf_iterator<char>(), std::back_inserter(cube_state));
	file_in.close();

	unsigned int k = 0;
	for(int j = 0; j < 15; ++j) {
		for(int i = 0; i < 12; ++i) {
			cube.state[j][i] = cube_state[k++];
		}
	}
	for(; k < cube_state.size(); ++k) {
		cube.moves.push_back((Moves) cube_state[k]);
	}

	return cube;
}

void CubeWrite(Cube cube, std::string file_path) {
	std::vector<char> cube_state;
	std::ofstream file_out(file_path, std::ios::out | std::ofstream::binary);

	if (!file_out)	{
		throw std::runtime_error("error opening file");
	}

	for(int j = 0; j < 15; ++j) {
		for(int i = 0; i < 12; ++i) {
			cube_state.push_back(cube.state[j][i]);
		}
	}
	for(unsigned int j = 0; j < cube.moves.size(); ++j) {
		cube_state.push_back((char)cube.moves[j]);
	}
	std::copy(cube_state.begin(), cube_state.end(), std::ostreambuf_iterator<char>(file_out));
	file_out.close();

	return;
}

void DrawCube(Cube cube) {
	std::cout << std::endl;
	for(unsigned int j = 0; j < 15; ++j) {
		std::cout << cube.state[j] << std::endl;
	}

	std::cout << std::endl << "permutations: ";
	for(unsigned int i = 0; i < cube.moves.size(); ++i) {
		if(i) {
			std::cout << ", ";
		}
		std::cout << PermutationTranslate(cube.moves[i]);
	}
	std::cout << std::endl << std::endl;
	return;
}

bool StateEval(Cube cube, bool draw) {
	std::stringstream sstream;
	bool partial = false;
	sstream << "\nSolved sides:   ";
	for(char side = 'A'; side <='F'; ++side) {
		if (SolvedSide(cube, side)) {
			sstream << side << ' ';
			partial = true;
		}
	}
	sstream << "\nSolved corners: ";
	for(char corner = 'A'; corner <='F'; ++corner) {
		if (SolvedCorners(cube, corner)) {
			sstream << corner << ' ';
			partial = true;
		}
	}
	sstream << "\nSolved edges:   ";
	for(char edge = 'A'; edge <='F'; ++edge) {
		if (SolvedEdges(cube, edge)) {
			sstream << edge << ' ';
			partial = true;
		}
	}
	if (Solved(cube)) {
		sstream << "\nCube state: Solved";
		partial = true;
	}
	sstream << "\n";
	if(partial == true && draw == true) {
		std::cout << sstream.str();
	}
	return partial;
}

std::string PermutationTranslate(Moves move) {
	switch(move) {
		default:
			return "Unknown";
		case PERMUTATION_IDENTITY:
			return "Identity";
		case PERMUTATION_E1L:
			return "E1L";
		case PERMUTATION_E2L:
			return "E2L";
		case PERMUTATION_E3L:
			return "E3L";
		case PERMUTATION_E1R:
			return "E1R";
		case PERMUTATION_E2R:
			return "E2R";
		case PERMUTATION_E3R:
			return "E3R";
		case PERMUTATION_A1U:
			return "A1U";
		case PERMUTATION_A2U:
			return "A2U";
		case PERMUTATION_A3U:
			return "A3U";
		case PERMUTATION_A1D:
			return "A1D";
		case PERMUTATION_A2D:
			return "A2D";
		case PERMUTATION_A3D:
			return "A3D";
		case PERMUTATION_B1L:
			return "B1L";
		case PERMUTATION_B2L:
			return "B2L";
		case PERMUTATION_B3L:
			return "B3L";
		case PERMUTATION_B1R:
			return "B1R";
		case PERMUTATION_B2R:
			return "B2R";
		case PERMUTATION_B3R:
			return "B3R";
	}
}

Cube A1U(Cube cube) {
	char tmp;
	for(int j = 0; j < 3; ++j)	{
		tmp = cube.state[j][4];
		cube.state[j    ][4] = cube.state[j +  4][4];
		cube.state[j + 4][4] = cube.state[j +  8][4];
		cube.state[j + 8][4] = cube.state[j + 12][4];
		cube.state[j + 12][4] = tmp;
	}

	for(int j = 0; j < 2; ++j)	{
		tmp = cube.state[4][0 + j];
		cube.state[4][0 + j] = cube.state[4 + j][2];
		cube.state[4 + j][2] = cube.state[6][2 - j];
		cube.state[6][2 - j] = cube.state[6 - j][0];
		cube.state[6 - j][0] = tmp;
	}

	cube.moves.push_back(PERMUTATION_A1U);

	return cube;
}

Cube A1D(Cube cube) {
	char tmp;
	for(int j = 0; j < 3; ++j)	{
		tmp = cube.state[j + 12][4];
		cube.state[j + 12][4] = cube.state[j + 8][4];
		cube.state[j +  8][4] = cube.state[j + 4][4];
		cube.state[j +  4][4] = cube.state[j    ][4];
		cube.state[j    ][4] = tmp;
	}

	for(int j = 0; j < 2; ++j)	{
		tmp = cube.state[6 - j][0];
		cube.state[6 - j][0] = cube.state[6][2 - j];
		cube.state[6][2 - j] = cube.state[4 + j][2];
		cube.state[4 + j][2] = cube.state[4][0 + j];
		cube.state[4][0 + j] = tmp;
	}

	cube.moves.push_back(PERMUTATION_A1D);

	return cube;
}

Cube A2U(Cube cube) {
	char tmp;
	for(int j = 0; j < 3; ++j)	{
		tmp = cube.state[j][5];
		cube.state[j    ][5] = cube.state[j +  4][5];
		cube.state[j + 4][5] = cube.state[j +  8][5];
		cube.state[j + 8][5] = cube.state[j + 12][5];
		cube.state[j + 12][5] = tmp;
	}

	cube.moves.push_back(PERMUTATION_A2U);

	return cube;
}

Cube A2D(Cube cube) {
	char tmp;
	for(int j = 0; j < 3; ++j)	{
		tmp = cube.state[j + 12][5];
		cube.state[j + 12][5] = cube.state[j + 8][5];
		cube.state[j +  8][5] = cube.state[j + 4][5];
		cube.state[j +  4][5] = cube.state[j    ][5];
		cube.state[j    ][5] = tmp;
	}

	cube.moves.push_back(PERMUTATION_A2D);

	return cube;
}

Cube A3U(Cube cube) {
	char tmp;
	for(int j = 0; j < 3; ++j)	{
		tmp = cube.state[j][6];
		cube.state[j    ][6] = cube.state[j +  4][6];
		cube.state[j + 4][6] = cube.state[j +  8][6];
		cube.state[j + 8][6] = cube.state[j + 12][6];
		cube.state[j + 12][6] = tmp;
	}

	for(int j = 0; j < 2; ++j)	{
		tmp = cube.state[4][10 - j];
		cube.state[4][10 - j] = cube.state[4 + j][8];
		cube.state[4 + j][8] = cube.state[6][ 8 + j];
		cube.state[6][ 8 + j] = cube.state[6 - j][10];
		cube.state[6 - j][10] = tmp;
	}

	cube.moves.push_back(PERMUTATION_A3U);

	return cube;
}

Cube A3D(Cube cube) {
	char tmp;
	for(int j = 0; j < 3; ++j)	{
		tmp = cube.state[j + 12][6];
		cube.state[j + 12][6] = cube.state[j + 8][6];
		cube.state[j +  8][6] = cube.state[j + 4][6];
		cube.state[j +  4][6] = cube.state[j    ][6];
		cube.state[j    ][6] = tmp;
	}

	for(int j = 0; j < 2; ++j)	{
		tmp = cube.state[6 - j][10];
		cube.state[6 - j][10] = cube.state[6][ 8 + j];
		cube.state[6][ 8 + j] = cube.state[4 + j][8];
		cube.state[4 + j][8] = cube.state[4][10 - j] ;
		cube.state[4][10 - j] = tmp;
	}

	cube.moves.push_back(PERMUTATION_A3D);

	return cube;
}

Cube B1L(Cube cube) {
	char tmp;

	for(int i = 0; i < 3; ++i) {
		tmp = cube.state[4][0 + i];
		cube.state[4][0 + i] = cube.state[4][4 + i];
		cube.state[4][4 + i] = cube.state[4][8 + i];
		cube.state[4][8 + i] = cube.state[14][6 - i];
		cube.state[14][6 - i] = tmp;
	}

	for(int j = 0; j < 2; ++j)	{
		tmp = cube.state[2 - j][4];
		cube.state[2 - j][4] = cube.state[2][6 - j];
		cube.state[2][6 - j] = cube.state[0 + j][6];
		cube.state[0 + j][6] = cube.state[0][4 + j];
		cube.state[0][4 + j] = tmp;
	}

	cube.moves.push_back(PERMUTATION_B1L);

	return cube;
}

Cube B1R(Cube cube) {
	char tmp;
	for(int i = 0; i < 3; i++) {
		tmp = cube.state[14][6 - i];
		cube.state[14][6 - i] = cube.state[4][8 + i];
		cube.state[4][8 + i] = cube.state[4][4 + i];
		cube.state[4][4 + i] = cube.state[4][0 + i];
		cube.state[4][0 + i] = tmp;
	}

	for(int j = 0; j < 2; ++j)	{
		tmp = cube.state[0][4 + j];
		cube.state[0][4 + j] = cube.state[0 + j][6];
		cube.state[0 + j][6] = cube.state[2][6 - j];
		cube.state[2][6 - j] = cube.state[2 - j][4];
		cube.state[2 - j][4] = tmp;
	}

	cube.moves.push_back(PERMUTATION_B1R);

	return cube;
}

Cube B2L(Cube cube) {
	char tmp;

	for(int i = 0; i < 3; ++i) {
		tmp = cube.state[5][0 + i];
		cube.state[5][0 + i] = cube.state[5][4 + i];
		cube.state[5][4 + i] = cube.state[5][8 + i];
		cube.state[5][8 + i] = cube.state[13][6 - i];
		cube.state[13][6 - i] = tmp;
	}

	cube.moves.push_back(PERMUTATION_B2L);

	return cube;
}

Cube B2R(Cube cube) {
	char tmp;
	for(int i = 0; i < 3; i++) {
		tmp = cube.state[13][6 - i];
		cube.state[13][6 - i] = cube.state[5][8 + i];
		cube.state[5][8 + i] = cube.state[5][4 + i];
		cube.state[5][4 + i] = cube.state[5][0 + i];
		cube.state[5][0 + i] = tmp;
	}

	cube.moves.push_back(PERMUTATION_B2R);

	return cube;
}

Cube B3L(Cube cube) {
	char tmp;

	for(int i = 0; i < 3; ++i) {
		tmp = cube.state[6][0 + i];
		cube.state[6][0 + i] = cube.state[6][4 + i];
		cube.state[6][4 + i] = cube.state[6][8 + i];
		cube.state[6][8 + i] = cube.state[12][6 - i];
		cube.state[12][6 - i] = tmp;
	}

	for(int j = 0; j < 2; ++j)	{
		tmp = cube.state[8][4 + j];
		cube.state[8][4 + j] = cube.state[8 + j][6];
		cube.state[8 + j][6] = cube.state[10][6 - j];
		cube.state[10][6 - j] = cube.state[10 - j][4];
		cube.state[10 - j][4] = tmp;
	}

	cube.moves.push_back(PERMUTATION_B3L);

	return cube;
}

Cube B3R(Cube cube) {
	char tmp;

	for(int i = 0; i < 3; ++i) {
		tmp = cube.state[12][6 - i];
		cube.state[12][6 - i] = cube.state[6][8 + i];
		cube.state[6][8 + i] = cube.state[6][4 + i];
		cube.state[6][4 + i] = cube.state[6][0 + i];
		cube.state[6][0 + i] = tmp;
	}

	for(int j = 0; j < 2; ++j)	{
		tmp = cube.state[10 - j][4];
		cube.state[10 - j][4] = cube.state[10][6 - j];
		cube.state[10][6 - j] = cube.state[8 + j][6];
		cube.state[8 + j][6] = cube.state[8][4 + j];
		cube.state[8][4 + j] = tmp;
	}

	cube.moves.push_back(PERMUTATION_B3R);

	return cube;
}

Cube E1L(Cube cube) {
	char tmp;
	for(int j = 0; j < 3; j++) {
		tmp = cube.state[4 + j][2];
		cube.state[4 + j][2] = cube.state[8][4+j];
		cube.state[8][4+j] = cube.state[6-j][8];
		cube.state[6-j][8] = cube.state[2][6-j];
		cube.state[2][6-j] = tmp;
	}

	for(int j = 0; j < 2; ++j)	{
		tmp = cube.state[6 - j][4];
		cube.state[6 - j][4] = cube.state[6][6 - j];
		cube.state[6][6 - j] = cube.state[4 + j][6];
		cube.state[4 + j][6] = cube.state[4][4 + j];
		cube.state[4][4 + j] = tmp;
	}

	cube.moves.push_back(PERMUTATION_E1L);

	return cube;
}

Cube E1R(Cube cube) {
	char tmp;
	for(int j = 0; j < 3; j++) {
		tmp = cube.state[2][6-j];
		cube.state[2][6-j] = cube.state[6-j][8];
		cube.state[6-j][8] = cube.state[8][4+j];
		cube.state[8][4+j] = cube.state[4 + j][2];
		cube.state[4 + j][2] = tmp;
	}

	for(int j = 0; j < 2; ++j)	{
		tmp = cube.state[4][4 + j];
		cube.state[4][4 + j] = cube.state[4 + j][6];
		cube.state[4 + j][6] = cube.state[6][6 - j];
		cube.state[6][6 - j] = cube.state[6 - j][4];
		cube.state[6 - j][4] = tmp;
	}

	cube.moves.push_back(PERMUTATION_E1R);

	return cube;
}

Cube E2L(Cube cube) {
	char tmp;
	for(int j = 0; j < 3; j++) {
		tmp = cube.state[4 + j][1];
		cube.state[4 + j][1] = cube.state[9][4+j];
		cube.state[9][4+j] = cube.state[6-j][9];
		cube.state[6-j][9] = cube.state[1][6-j];
		cube.state[1][6-j] = tmp;
	}

	cube.moves.push_back(PERMUTATION_E2L);

	return cube;
}

Cube E2R(Cube cube) {
	char tmp;
	for(int j = 0; j < 3; j++) {
		tmp = cube.state[1][6-j];
		cube.state[1][6-j] = cube.state[6-j][9];
		cube.state[6-j][9] = cube.state[9][4+j];
		cube.state[9][4+j] = cube.state[4 + j][1];
		cube.state[4 + j][1] = tmp;
	}

	cube.moves.push_back(PERMUTATION_E2R);

	return cube;
}

Cube E3L(Cube cube) {
	char tmp;
	for(int j = 0; j < 3; j++) {
		tmp = cube.state[4 + j][0];
		cube.state[4 + j][0] = cube.state[10][4+j];
		cube.state[10][4+j] = cube.state[6-j][10];
		cube.state[6-j][10] = cube.state[0][6-j];
		cube.state[0][6-j] = tmp;
	}

	for(int j = 0; j < 2; ++j)	{
		tmp = cube.state[12][4 + j];
		cube.state[12][4 + j] = cube.state[12 + j][6];
		cube.state[12 + j][6] = cube.state[14][6 - j];
		cube.state[14][6 - j] = cube.state[14 - j][4];
		cube.state[14 - j][4] = tmp;
	}

	cube.moves.push_back(PERMUTATION_E3L);

	return cube;
}

Cube E3R(Cube cube) {
	char tmp;
	for(int j = 0; j < 3; j++) {
		tmp = cube.state[0][6-j];
		cube.state[0][6-j] = cube.state[6-j][10];
		cube.state[6-j][10] = cube.state[10][4+j];
		cube.state[10][4+j] = cube.state[4 + j][0];
		cube.state[4 + j][0] = tmp;
	}

	for(int j = 0; j < 2; ++j)	{
		tmp = cube.state[14 - j][4];
		cube.state[14 - j][4] = cube.state[14][6 - j];
		cube.state[14][6 - j] = cube.state[12 + j][6];
		cube.state[12 + j][6] = cube.state[12][4 + j];
		cube.state[12][4 + j] = tmp;
	}

	cube.moves.push_back(PERMUTATION_E3R);

	return cube;
}

bool SolvedSide(Cube cube, char side) {
	switch(side) {
		default:
			return false;
		case 'A':
			return SolvedSide(cube, 0, 4);
		case 'B':
			return SolvedSide(cube, 4, 0);
		case 'C':
			return SolvedSide(cube, 4, 4);
		case 'D':
			return SolvedSide(cube, 4, 8);
		case 'E':
			return SolvedSide(cube, 8, 4);
		case 'F':
			return SolvedSide(cube, 12, 4);
	}
}

bool SolvedSide(Cube cube, int offset_y, int offset_x) {
	for(int j = 0; j < 3; ++j) {
		for(int i = 0; i < 3; ++i) {
			if(cube.state[offset_y][offset_x] != cube.state[j + offset_y][i + offset_x]) {
				return false;
			}
		}
	}
	return true;
}

bool SolvedCorners(Cube cube, char side) {
	switch(side) {
		default:
			return false;
		case 'A':
			return ((cube.state[0][4] == cube.state[0][6] ) && (cube.state[2][4] == cube.state[2][6]) && (cube.state[0][4] == cube.state[2][4]) &&
					(cube.state[4][0] == cube.state[4][2] ) &&
					(cube.state[4][4] == cube.state[4][6] ) &&
					(cube.state[4][8] == cube.state[4][10]) &&
					(cube.state[14][4] == cube.state[14][6]));
		case 'B':
			return ((cube.state[4][0] == cube.state[4][2]) && (cube.state[6][0] == cube.state[6][2]) && (cube.state[4][0] == cube.state[6][0]) &&
					(cube.state[0][4] == cube.state[2][4]) &&
					(cube.state[4][4] == cube.state[6][4]) &&
					(cube.state[8][4] == cube.state[10][4]) &&
					(cube.state[12][4] == cube.state[14][4]));
		case 'C':
			return ((cube.state[4][4] == cube.state[4][6]) && (cube.state[6][4] == cube.state[6][6]) && (cube.state[4][4] == cube.state[6][4]) &&
					(cube.state[2][4] == cube.state[2][6]) &&
					(cube.state[4][2] == cube.state[6][2]) &&
					(cube.state[4][8] == cube.state[6][8]) &&
					(cube.state[8][4] == cube.state[8][6]));
		case 'D':
			return ((cube.state[4][8] == cube.state[4][10]) && (cube.state[6][8] == cube.state[6][10]) && (cube.state[4][8] == cube.state[6][8]) &&
					(cube.state[0][6] == cube.state[2][6]) &&
					(cube.state[4][6] == cube.state[6][6]) &&
					(cube.state[8][6] == cube.state[10][6]) &&
					(cube.state[12][6] == cube.state[14][6]));
		case 'E':
			return ((cube.state[8][4] == cube.state[8][6]) && (cube.state[10][4] == cube.state[10][6]) && (cube.state[8][4] == cube.state[10][4]) &&
					(cube.state[6][0] == cube.state[6][2]) &&
					(cube.state[6][4] == cube.state[6][6]) &&
					(cube.state[6][8] == cube.state[6][10]) &&
					(cube.state[12][4] == cube.state[12][6]));
		case 'F':
			return ((cube.state[12][4] == cube.state[12][6]) && (cube.state[14][4] == cube.state[14][6]) && (cube.state[12][4] == cube.state[14][4]) &&
					(cube.state[0][4] == cube.state[0][6]) &&
					(cube.state[4][0] == cube.state[6][0]) &&
					(cube.state[4][10] == cube.state[6][10]) &&
					(cube.state[8][4] == cube.state[8][6]));
	}
	return true;
}

bool SolvedCorners(Cube cube) {
	for(char side = 'A'; side <= 'F'; ++side) {
		if(SolvedCorners(cube, side) == false) {
			return false;
		}
	}
	return true;
}

bool SolvedEdges(Cube cube, char edge) {
	switch(edge) {
		default:
			return false;
		case 'A':
			return ((cube.state[0][5] == cube.state[1][4]) && (cube.state[1][6] == cube.state[2][5]) && (cube.state[0][5] == cube.state[1][6]) &&
					(((cube.state[4][1] < cube.state[4][5]) && (cube.state[4][5] < cube.state[4][9]) && (cube.state[4][9] < cube.state[14][5])) ||
					((cube.state[4][5] < cube.state[4][9]) && (cube.state[4][9] < cube.state[14][5]) && (cube.state[14][5] < cube.state[4][1])) ||
					((cube.state[4][9] < cube.state[14][5]) && (cube.state[14][5] < cube.state[4][1]) && (cube.state[4][1] < cube.state[4][5])) ||
					((cube.state[14][5] < cube.state[4][1]) && (cube.state[4][1] < cube.state[4][5]) && (cube.state[4][5] < cube.state[4][9]))));
		case 'B':
			return ((cube.state[4][1] == cube.state[5][0]) && (cube.state[5][2] == cube.state[6][1]) && (cube.state[4][1] == cube.state[5][2]) &&
					(((cube.state[1][4] < cube.state[5][4]) && (cube.state[5][4] < cube.state[9][4]) && (cube.state[9][4] < cube.state[13][4])) ||
					((cube.state[5][4] < cube.state[9][4]) && (cube.state[9][4] < cube.state[13][4]) && (cube.state[13][4] < cube.state[1][4])) ||
					((cube.state[9][4] < cube.state[13][4]) && (cube.state[13][4] < cube.state[1][4]) && (cube.state[1][4] < cube.state[5][4])) ||
					((cube.state[13][4] < cube.state[1][4]) && (cube.state[1][4] < cube.state[5][4]) && (cube.state[5][4] < cube.state[9][4]))));
		case 'C':
			return ((cube.state[4][5] == cube.state[5][4]) && (cube.state[5][6] == cube.state[6][5]) && (cube.state[4][5] == cube.state[5][6]) &&
					(((cube.state[2][5] < cube.state[5][2]) && (cube.state[5][2] < cube.state[5][8]) && (cube.state[5][8] < cube.state[8][5])) ||
					((cube.state[5][2] < cube.state[8][5]) && (cube.state[8][5] < cube.state[2][5]) && (cube.state[2][5] < cube.state[5][8])) ||
					((cube.state[8][5] < cube.state[5][8]) && (cube.state[5][8] < cube.state[5][2]) && (cube.state[5][2] < cube.state[2][5])) ||
					((cube.state[5][8] < cube.state[2][5]) && (cube.state[2][5] < cube.state[8][5]) && (cube.state[8][5] < cube.state[5][2]))));
		case 'D':
			return ((cube.state[4][9] == cube.state[5][8]) && (cube.state[5][10] == cube.state[6][9]) && (cube.state[4][9] == cube.state[5][10]) &&
					(((cube.state[1][6] < cube.state[5][6]) && (cube.state[5][6] < cube.state[9][6]) && (cube.state[9][6] < cube.state[13][6])) ||
					((cube.state[5][6] < cube.state[9][6]) && (cube.state[9][6] < cube.state[13][6]) && (cube.state[13][6] < cube.state[1][6])) ||
					((cube.state[9][6] < cube.state[13][6]) && (cube.state[13][6] < cube.state[1][6]) && (cube.state[1][6] < cube.state[5][6])) ||
					((cube.state[13][6] < cube.state[1][6]) && (cube.state[1][6] < cube.state[5][6]) && (cube.state[5][6] < cube.state[9][6]))));
		case 'E':
			return ((cube.state[8][5] == cube.state[9][4]) && (cube.state[9][6] == cube.state[10][5]) && (cube.state[8][5] == cube.state[9][6]) &&
					(((cube.state[6][1] < cube.state[6][5]) && (cube.state[6][5] < cube.state[6][9]) && (cube.state[6][9] < cube.state[12][5])) ||
					((cube.state[6][5] < cube.state[6][9]) && (cube.state[6][9] < cube.state[12][5]) && (cube.state[12][5] < cube.state[6][1])) ||
					((cube.state[6][9] < cube.state[12][5]) && (cube.state[12][5] < cube.state[6][1]) && (cube.state[6][1] < cube.state[6][5])) ||
					((cube.state[12][5] < cube.state[6][1]) && (cube.state[6][1] < cube.state[6][5]) && (cube.state[6][5] < cube.state[6][9]))));
		case 'F':
			return ((cube.state[12][5] == cube.state[13][4]) && (cube.state[13][6] == cube.state[14][5]) && (cube.state[12][5] == cube.state[13][6]) &&
					(((cube.state[0][5] < cube.state[5][0]) && (cube.state[5][0] < cube.state[5][10]) && (cube.state[5][10] < cube.state[10][5])) ||
					((cube.state[5][10] < cube.state[0][5]) && (cube.state[0][5] < cube.state[10][5]) && (cube.state[10][5] < cube.state[5][0])) ||
					((cube.state[10][5] < cube.state[5][10]) && (cube.state[5][10] < cube.state[5][0]) && (cube.state[5][0] < cube.state[0][5])) ||
					((cube.state[5][0] < cube.state[10][5]) && (cube.state[10][5] < cube.state[0][5]) && (cube.state[0][5] < cube.state[5][10]))));
	}
	return true;
}

bool SolvedEdges(Cube cube) {
	for(char side = 'A'; side <= 'F'; ++side) {
		if(SolvedEdges(cube, side) == false) {
			return false;
		}
	}
	return true;
}

bool Solved(Cube cube) {
	for(char side = 'A'; side <= 'F'; ++side) {
		if(SolvedSide(cube, side) == false) {
			return false;
		}
	}
	return true;
}

Cube ApplyInverse(Cube cube, Moves permutation) {
	return Apply(cube,  (Moves) -permutation);
}

Cube Apply(Cube cube, std::vector<char> permutations) {
	for(unsigned int i = 0; i < permutations.size(); i++) {
		cube = Apply(cube, permutations[i]);
	}
	return cube;
}

Cube Apply(Cube cube, char permutation) {
	return Apply(cube, (Moves) permutation);
}

Cube Apply(Cube cube, Moves permutation) {
	switch(permutation) {
		default:
			break;
		case PERMUTATION_A1U:
			cube = A1U(cube);
			break;
		case PERMUTATION_A1D:
			cube = A1D(cube);
			break;
		case PERMUTATION_A2U:
			cube = A2U(cube);
			break;
		case PERMUTATION_A2D:
			cube = A2D(cube);
			break;
		case PERMUTATION_A3U:
			cube = A3U(cube);
			break;
		case PERMUTATION_A3D:
			cube = A3D(cube);
			break;
		case PERMUTATION_B1L:
			cube = B1L(cube);
			break;
		case PERMUTATION_B1R:
			cube = B1R(cube);
			break;
		case PERMUTATION_B2L:
			cube = B2L(cube);
			break;
		case PERMUTATION_B2R:
			cube = B2R(cube);
			break;
		case PERMUTATION_B3L:
			cube = B3L(cube);
			break;
		case PERMUTATION_B3R:
			cube = B3R(cube);
			break;
		case PERMUTATION_E1L:
			cube = E1L(cube);
			break;
		case PERMUTATION_E1R:
			cube = E1R(cube);
			break;
		case PERMUTATION_E2L:
			cube = E2L(cube);
			break;
		case PERMUTATION_E2R:
			cube = E2R(cube);
			break;
		case PERMUTATION_E3L:
			cube = E3L(cube);
			break;
		case PERMUTATION_E3R:
			cube = E3R(cube);
			break;
	}
	return cube;
}

Cube Scramble(Cube cube) {
	static std::random_device rd;
	static std::default_random_engine generator(rd());
	static std::uniform_int_distribution<int> rand_permutation(PERMUTATION_E3R, PERMUTATION_E3L);
	static std::uniform_int_distribution<int> total_permutations(0, 4096);

	for(int j = total_permutations(generator); j > 0; --j) {
		Moves tmp = PERMUTATION_IDENTITY;
		while(tmp == PERMUTATION_IDENTITY) {
			tmp = (Moves) rand_permutation(generator);
		}
		if(cube.moves.size() > 0) {
			if(cube.moves.back() != -tmp) {
				cube = Apply(cube, tmp);
			}
		}
		else {
			cube = Apply(cube, tmp);
		}
	}
	return cube;
}

Cube TraceBack(Cube cube) {
	Moves last = cube.moves.back();
	cube = ApplyInverse(cube, last);
	cube.moves.pop_back();
	cube.moves.pop_back();

	return cube;
}

Cube Solve(Cube cube) {
//TODO: "Just" Implement Solve... :-)
	return cube;
}

std::vector<std::vector<char> > PermutationsLoad(std::string permutations_file) {
	std::vector<std::vector<char> > permutations_vector;
	std::vector<char> permutation;
	std::vector<char> file_slurp;

	std::ifstream file_in(permutations_file, std::ios_base::binary);
	if (file_in) {
		std::istreambuf_iterator<char> iter(file_in);
		std::copy(iter, std::istreambuf_iterator<char>(), std::back_inserter(file_slurp));
		file_in.close();

		permutation.clear();
		permutations_vector.clear();
		for(unsigned int j = 0; j < file_slurp.size(); ++j) {
			if(file_slurp[j] == 0) {
				permutations_vector.push_back(permutation);
				permutation.clear();
			}
			else {
				permutation.push_back(file_slurp[j]);
			}
		}
	}
	return permutations_vector;
}

void PermutationsSave(std::vector<std::vector<char> > permutations, std::string permutations_file) {
	std::ofstream file_out(permutations_file, std::ios::out | std::ofstream::binary);
	if (!file_out)	{
		throw std::runtime_error("error opening file");
	}
	else {
		std::vector<char> tmp;
		for(unsigned int j = 0; j < permutations.size(); ++j) {
			for(unsigned int i = 0; i < permutations[j].size(); ++i) {
				tmp.push_back(permutations[j][i]);
			}
			tmp.push_back(0);
		}
		std::copy(tmp.begin(), tmp.end(), std::ostreambuf_iterator<char>(file_out));
		file_out.close();
	}
	return;
}

void DrawPermutation(std::vector<char> permutation) {
	for(unsigned int i = 0; i < permutation.size(); ++i) {
		std::cout << PermutationTranslate((Moves)permutation[i]) <<  ' ';
	}
	std::cout << std::endl;
}

void DrawPermutations(std::vector<std::vector<char> > permutations_vector) {
	for(unsigned int i = 0; i < permutations_vector.size(); ++i) {
		DrawPermutation(permutations_vector[i]);
	}
}

unsigned long int PermutationToId(std::vector<char> permutation) {
	unsigned long int id = 0;
	for(int i = (permutation.size() - 1); i >= 0; --i) {
		id *= BASE;
		if(permutation[i] > 0) {
			id += permutation[i] - 1;
		}
		else {
			id += (permutation[i] + BASE);
		}
	}
	return id;
}

std::vector<char> IdToPermutation(unsigned long int id) {
	std::vector<char> permutation;
	if(id == 0) {
		permutation.push_back((char) PERMUTATION_A1U);
	}
	else{
		for(; id > 0; id = id/BASE) {
			char perm;
			if(id%BASE < 9) {
				perm = (id%BASE) + 1;
			}
			else {
				perm = (id%BASE) - BASE;
			}
			permutation.push_back(perm);
		}
	}
	return permutation;
}

void IdentityMap(unsigned long int last_mapped_permutation_id) {
// TODO: Make this overload map identity permutations and save its state properly so to not loose solutions
	std::vector<std::vector<char> > identity_permutations, mined_identity_permutations;
	std::vector<char> permutation;

////	do {
////		load ini

////Loading Identity permutation sequences
		identity_permutations = PermutationsLoad("identities.perm");
		permutation.push_back((char)PERMUTATION_IDENTITY);
		identity_permutations.insert(identity_permutations.begin(), permutation);

//Printing all Loaded identity permutations
		std::cout << "Loaded Identity permutations: " << std::endl;
		DrawPermutations(identity_permutations);

		mined_identity_permutations = SolutionsMap(Reset(), identity_permutations, BASE*BASE*BASE*BASE*BASE, 0);
//		save ini
//Saving Identity permutation sequences
		identity_permutations.erase(identity_permutations.begin());
		identity_permutations.insert(identity_permutations.end(), mined_identity_permutations.begin(), mined_identity_permutations.end());
		PermutationsSave(identity_permutations, "identities.perm");
//		n += BASE*BASE*BASE;
////	} while(n < ini);
}

std::vector<std::vector<char> > SolutionsMap(Cube original_cube, std::vector<std::vector<char> > identity_permutations, unsigned long int n, unsigned long int ini) {
	std::vector<std::vector<char> > mined_identity_permutations;
	std::vector<char> permutation;
	Cube cube;

//	Going through all possible former n combinations saving identities
	std::cout << "Mapping new Identities: " << std::endl;
	n += ini;
	for(unsigned long int j = ini; j < n; ++j) {
		permutation = IdToPermutation(j);
		if((CheckIdentitySubsequence(permutation, identity_permutations) == false) && (CheckIdentitySubsequence(permutation, mined_identity_permutations) == false)) {
			cube = Apply(original_cube, permutation);
			if(Solved(cube)) {
				mined_identity_permutations.push_back(permutation);
				DrawPermutation(permutation);
			}
		}
	}
	return mined_identity_permutations;
}

bool VectorSearch(std::vector<char> string, std::vector<char> substring) {	//TODO: Optimize this with substring search optimized code (Sedgewick, Knuth, etc.)
	for(unsigned int j = 0; j < string.size(); ++j) {
		if(string.size() - j < substring.size()) {
			break;
		}
		unsigned int i;
		for(i = 0; i < substring.size(); ++i) {
			if(string[j + i] != substring[i]) {
				break;
			}
		}
		if(i >= substring.size()) {
			return true;
		}
	}
	return false;
}

bool CheckIdentitySubsequence(std::vector<char> permutation, std::vector<std::vector<char> > identity_permutations) {
	for(unsigned int i = 0; i < identity_permutations.size(); ++i) {
		if(VectorSearch(permutation, identity_permutations[i]) == true) {
			return true;
		}
	}
	return false;
}

int main(int, char**) {
	Cube cube;

	std::cout<< "Inital state: " << std::endl;
	cube = Reset();
	DrawCube(cube);
	StateEval(cube);

	std::cout<< "\nScrambled state: " << std::endl;
	cube = Scramble(cube);
	DrawCube(cube);
	StateEval(cube);

	std::cout<< "Tracing back and listing relevant intermadiate states..." << std::endl;
	while(cube.moves.size()) {
		cube = TraceBack(cube);
		if(StateEval(cube)) {
			DrawCube(cube);
		}
	}
	DrawCube(cube);

	cube = SetCornersSwap();
	DrawCube(cube);
	CubeWrite(cube, "test.cube");

	cube = Reset();
	cube = CubeRead("test.cube");
	DrawCube(cube);

	std::cout<< "Creating States Map..." << std::endl;
	IdentityMap(BASE*BASE*BASE*BASE);
	return 0;
}
