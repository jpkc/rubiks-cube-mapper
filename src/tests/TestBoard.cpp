#include "../Board.h"
#include <iostream>

int TestConstructor()
{
    std::cout << "Testing Constuctor and Destructor. ";
    Board board;
    std::cout << "Board Size: " << board.size() << std::endl;
    return 1;
}

int Test_get()
{
    Board board;
    std::array<int, BOARD_SIZE> board_array = board.get();

    std::cout << "Testing get: ";
    for (const auto &e : board_array)
    {
        std::cout << e << ", ";
    }
    std::cout << std::endl;
    return 1;
}

int Test_set()
{
    Board board;
    std::array<int, BOARD_SIZE> board_array;

    std::cout << "Testing set: ";
    for (int i = 0; i < BOARD_SIZE; ++i)
    {
        board_array[i] = BOARD_SIZE - i;
    }
    board.set(board_array);
    std::cout << (board_array == board.get()) << std::endl;
    return 1;
}

int main()
{
    int result = 1;

    result *= TestConstructor();
    result *= Test_get();
    result *= Test_set();

    return result;
}
