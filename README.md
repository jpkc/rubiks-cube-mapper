# Goals:

- Describe Problem
- Handle Permutations
- Map sequence of permutations
- identify ***isomorphisms***
- Naively solve with ***backtracking*** (rolling back permutations)
- Solve with mapped permutations (without knowing how the problem got there. In other words, without ***backtracking***)
- Move solver to **GPU**
- Run solver in parallel

# milestones:

## Describe problem as a data structure

- Save the cube state in memory
- Verify that the cube is finished

## Make permutations

- Change pieces positions on the cube (add transpositions (valid permutations))
- Verify isomorphisms

## Map Permutations

- Map sequences of permutations
- Avoid isomorphisms and its branches (this will head to minimal step solutions)

## Back Tracking

- Find a solution for any given valid state (permutation)

## Check for valid permutations

- Check if current state is valid. (how?)
 
## Move to GPU

- Describe problem and its permutations as Matrices
- Move operations to GPU


> C++ solution
> Small memory footprint for solving
> Might have a huge memory footprint while searching for permuations


# Describing The problem

## Text file description

### The Cube (Board)

The cube can be described with the following text file:

``problem map.txt``

```
    123
    456
    789
   
abc jkl stu
def mno vwx
ghi pqr yzA

    BCD
    EFG
    HIJ
    
    KLM
    NOP
    QRS
```

The Cube is a problem that can be described with ``9 * 6 = 54`` char values.

### The Permutations

Permutations can be describes as a rules to move the chars around in a valid way.

Turning the left side of the cube changes will change the problem map in the following way:

``problem map.txt``

```
    K23
    N56
    Q89
   
gda 1kl stu
heb 4no vwx
ifc 7qr yzA

    jCD
    mFG
    pIJ
    
    BLM
    EOP
    HRS
```

The same can be done to all 18 possible cube single permutations

## Matrix Representation

If we manage to properly represent our problem with matrices, we might be able to process it on GPU.

For that we would like to represent the board as a matrix and the permutations as matrix multiplication. The way to do so is through [permutation matrices](https://www.stata.com/manuals13/m-1permutation.pdf).

Permutation matrices are built swapping rows and columns from the Identity Matrix. That way, when you multiply it by a vector, you will swap some positions. For this to work we will have to translate our ``problem map.txt`` as a one dimensional vector,end prepare the proper permutation matrices.

``problem map as a vector.txt``
``` 
[[1], [2], [3], [4], [5], [6], [7], [8], [9], [a], [b], [c], ... , [K], [L], [M], [N], [O], [P], [Q], [R], [S]]
```

This means that not only the Cube can be described as a Matrix problem, but any permutation problem. With this **we solve whatever permutation problems we might be interested in**, and we can do so powered by GPUs.

Our Cube is represented as a board with ``9 * 6 = 54`` positions that can be somehow shuffled. In matrix mode this boils down to a vector with 54 dimensions, and the permutation matrices will be a Identity Matrix with side 54 with their rows and columns shuffled as needed.

# Mapping Permutations

## Fundamental Permutations: The π/4 (90&deg;) turns

There are 12 possible moves in each side:

- Each column 90&deg; up
- Each column 90&deg; down
- Each row 90&deg; left
- Each row 90&deg; right

## Permutation Notation

Facing the cube, you can ***roll** **Up** or **Down** the **Left**, **Center** or **Right** slices.
Out notation will be PXX-S-D:

- **PXX**, where **XX** is the permutation number
- **S**, where **S** is the **Slice** to be rotated
- **D**, where **D** is the **Direction** the Slice will move

**Isomorphisms removed**, it adds up to a total of **18** unique 90&deg; rotations.

# Complexity Analysis

## The Algorithm itself

We are talking about mapping all cube states. The intentions is to do so through a digraph (tree) placing the solved bube in the root and applying the 18 basic permutations to get the first bach of leaves.

After that, we intend to apply all possible permutations to all leaves that are not isomorphisms (i.e. leads to or is an isomorphism of a previously known).

 Today is known that you need at most 20 movements to solve the 3x3x3 Rubik's cube. This immediately leads to a technological problem, as the possible permutations are so many that there is no way to map all that. For every algorithm iteration we will get at most 18 new branches from each leaf. This gives us at most 18²⁰ = 1,338258845×10³⁰ states to build and keep track of, which is itself 20 orders of magnitude bigger than the available memory on today's computers.

Of course there are isomorphisms, but the number of states is humangous still. Hou can we narrow our down state tree?

## Narrowing down the possibilities

The rubiks cube itself composed by three puzzles.

- The 6 center pieces (Always aligned in a 3x3 or odd sized cube, but splits in many more for bigger cubes)
- The 8 corner pieces (which are always solvable in the same number of movements for whaterer Cube size)
- The 12 edge pieces (splits in many more for bigger cubes)

We solve it puzzle by puzle providing that we can sinc the tree so that they match in the end.

Considering this, we can get tackle the problem splitting it into 3 smaller trees. We can also find some key permutations for each one of those and use them instead of the 90⁰ permutations, significantly narrowing down our search.

It turns out that we only need 4 permutations to fix the edges and 4 permutations to fix the corner, each one composed by 8x 90⁰ movements each.

Building those composed permutations is key to find an efficient solution.

## Searching for the composed permutations

The composed permutations can be identifyed by the time they take to return to the original position. If you repeat them 3 times, you should return to the original state. That means that we can take all permutation generated from 8 basic movements and repeat them 3 times to test.

We also get their inverse by applying them two times and checking which other permutations gets there in only one go.

## How well does this narrow down the search?

Splitting the problem into 3 trees replaces 8 levels of our state tree by 1 level. We also narrowed down the search to at most 4 permutations for each new leaf. This is a huge game changer: We can now map only the relevant states from the previous 18²⁰. We are now talking about 3 levels spawned from 4 distinct (composed) permutations, giving us 4³ - 64 total states to keep track of in each tree.

This takes us to the current human algorithms:

- Some initial movements to get Centers, Corners and Edges in sync
- Fix all corners
- Fix all edges

Well, this looks like a plan. Lets get our hands dirty!

# Current Tasks

- [X] Add README.md file
- [X] Add basic Theory on README.md
- [X] Get simple Eigen example working
- [X] Get simple permutation working as Matrix Multiplication
- [ ] Create all 18 basic permutation matrices
- [ ] Move tests to Google Test
- [ ] Create Cube State **Importer** (``cube_state.txt`` -> ``Vector Matrix``)
- [ ] Create Cube State **Exporter** (``Vector Matrix`` -> ``cube_state.txt``)
- [ ] Provide a way to *reposition* cube so that we are always looking it from the same position (minding that centers are always in the same relative position to each other)
- [ ] Provide a way to check if cube is solved (Reposition and compare with solution/check if Cube/Board Vector is ordered from 1, 2, ..., 54)

# Next Steps

- [ ] Build some type of Digraph (tree) where leafs are **Cube States**. Root is a **Solved Cube**
- [ ] Apply all 18 basic permutations to every leaf of the State Digraph (Breadth First Search (BFS) over **Cube States**)
- [ ] Save/Map states after permutations to add to graph nodes
- [ ] Prevent expanding leafs that lead to **Isomorphism**
- [ ] Calculate number of possible moves. Best solution is said to take at most 24 movements.
- [ ] Calculate Memory complexity of solution
- [ ] Calculate CPU complexity of solution
- [ ] Work out a Memoized Solution comparing current state to nodes before expanding leaves with new basic 18 permutations
- [ ] Build Digraph in such a way that it knows what was the last permutation so to backtrack the movements all the way to the to graph Root (go up the tree to solve the cube) and also preventing leaf expansion to the inverse of the last permutation












